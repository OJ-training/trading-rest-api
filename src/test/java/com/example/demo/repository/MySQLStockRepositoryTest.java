package com.example.demo.repository;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Stock;
import com.example.demo.entities.StockPrices;
import com.example.demo.entities.Statement;
// @ActiveProfiles("h2")
@SpringBootTest
public class MySQLStockRepositoryTest {
	
	@Autowired
	MySQLStockRepository mySQLRepo;
	
	@Test
	public void testGetPortfolio() {
		System.out.println("test running");
		List<Portfolio> returnedList = mySQLRepo.getPortfolio();
		assert(returnedList != null);
		for(Portfolio pos: returnedList) {
			System.out.println(pos.getStockTticker());
		}	
		
	}
	@Test
	public void  testGetTradingHistory(){
		System.out.println("test running");
		List<Stock> returnedList = mySQLRepo.getTradingHistory();
		assert(returnedList != null);
		for(Stock stocks: returnedList) {
			System.out.println(stocks.getStockTicker());
		}	
		
	}
	@Test
	public void  testGetStockPrices(){
		System.out.println("test running");
		List<StockPrices> returnedList = mySQLRepo.getStockPrices();
		assert(returnedList != null);
		for(StockPrices stocks: returnedList) {
			System.out.println(stocks.getTickerName());
		}	
		
	}
	@Test
	public void  testGetStatement(){
		System.out.println("test running");
		Statement returned = mySQLRepo.getStatement();
		assert(returned != null);
		
		System.out.println(returned.getMaxProfit());
	
		
	}

}


