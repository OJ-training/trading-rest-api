package com.example.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Statement;
import com.example.demo.entities.Stock;
import com.example.demo.entities.StockPrices;
import com.example.demo.entities.Stock;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

//ActiveProfiles("h2")
@SpringBootTest
@AutoConfigureMockMvc
public class StockControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testShouldReturnHistoryListSize() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stocks/history"))
							              .andDo(print())
							              .andExpect(status().isOk())
							              .andReturn();
		
		List<Stock> trades = new ObjectMapper().readValue(
											mvcResult.getResponse().getContentAsString(),
				                            new TypeReference<List<Stock>>() { });
		
		assertThat(trades.size()).isGreaterThan(0);
	}
	
	@Test
	public void testShouldReturnPricesList() throws Exception{
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stocks/prices"))
			              .andDo(print())
			              .andExpect(status().isOk())
			              .andReturn();
		
		List<StockPrices> prices = new ObjectMapper().readValue(
							mvcResult.getResponse().getContentAsString(),
		                  new TypeReference<List<StockPrices>>() { });
		
		assertThat(prices.size()).isGreaterThan(0);
	}
	
	@Test
	public void testShouldReturnPortfolioList() throws Exception{
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stocks/portfolio"))
			              .andDo(print())
			              .andExpect(status().isOk())
			              .andReturn();
		
		List<Portfolio> portfolio = new ObjectMapper().readValue(
							mvcResult.getResponse().getContentAsString(),
		                  new TypeReference<List<Portfolio>>() { });
		
		assertThat(portfolio.size()).isGreaterThan(0);
	}
	
	@Test
	public void testShouldReturnStatment() throws Exception{
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stocks/statement"))
			              .andDo(print())
			              .andExpect(status().isOk())
			              .andReturn();
		
		Statement statement = new ObjectMapper().readValue(
							mvcResult.getResponse().getContentAsString(),
		                  new TypeReference<Statement>() { });
		
		assertThat(statement.getMaxProfit()).isGreaterThanOrEqualTo(0);
	}
}
