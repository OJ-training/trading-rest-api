package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Statement;
import com.example.demo.entities.Stock;
import com.example.demo.repository.StockRepository;
@SpringBootTest
public class StockServiceTest {

		@Autowired
		StockService stockService;
		@MockBean
		StockRepository stockRepository;		
		@Test
		public void testGetStatement() {
			Statement testStatement = new Statement(1000,-1000,2000,-2000);
			when(stockRepository.getStatement())
			.thenReturn(testStatement);
			assertThat(testStatement.getMaxProfit() ).isEqualTo(1000);
		}
		@Test
		public void testGetPortfolio() {
			List<Portfolio> mockList = new ArrayList<Portfolio>();
			when(stockRepository.getPortfolio())
			.thenReturn(mockList);
			assertThat(stockRepository.getPortfolio() ).isEqualTo(mockList);
			 
		}

		@Test
		public void testGetTradingHostory() {
			List<Stock> mockList = new ArrayList<Stock>();
			when(stockRepository.getTradingHistory())
			.thenReturn(mockList);
			assertThat(stockRepository.getPortfolio() ).isEqualTo(mockList);
			 
		}
		
}
