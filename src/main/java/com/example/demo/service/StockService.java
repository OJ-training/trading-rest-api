package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.demo.entities.Stock;
import com.example.demo.entities.CompanyData;
import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Statement;
import com.example.demo.entities.StockPrices;
import com.example.demo.repository.StockRepository;

@Service
public class StockService {

	@Autowired
	private StockRepository repository;
	

	public List<Stock> getTradingHistory() {
		return repository.getTradingHistory();
	}

	public List<Portfolio> getPortfolio() {
		return repository.getPortfolio();
	}
	public List<StockPrices> getStockPrices() {
		return repository.getStockPrices();
	}
	public Stock newStock(Stock stock) {
		return repository.addStock(stock);
	}
	public Statement getStatement(){
		return repository.getStatement();
	}
	

	@Autowired
	private RestTemplate restTemplate;
	
	private static String url ="http://api.marketstack.com/v1/eod?access_key=ffec2cbe138dbeeb27de476a2cf1e36e&symbols=AAPL&limit=1";
	
	public double getStockLivePrice(String ticker) {
		UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromUriString(url);
		urlBuilder.replaceQueryParam("symbols", ticker);
		String result = urlBuilder.build().toUriString();
		CompanyData stockPrices = restTemplate.getForObject(result, CompanyData.class);
		
		double price = stockPrices.getData().get(0).getClose();
		repository.updateLiveStockPrice(ticker,price);
		return price;
	}
	
	/*
	public List<Stock> getStockViaTickerOnly(String ticker) {
		return repository.getStockByTickerOnly(ticker);
	}

	public List<Stock> getStockViaTicker(String ticker, String date) {
		return repository.getStockByTicker(ticker, date);
	}

	public String deleteStock(String ticker, String date) {
		return repository.deleteStock(ticker, date);
	}
	*/

}
