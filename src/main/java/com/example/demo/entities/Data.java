package com.example.demo.entities;

public class Data {
	private double open;
	private double high;
	private double low;
	private double close;
	private double volume;

	private double adj_high;
	private double adj_low;
	private double adj_close;
	private double adj_open;
	private double adj_volume;
	
	private double split_factor;
	private String symbol;
	private String exchange;
	public Data(double open, double high, double low, double close, double volume, double adj_high, double adj_low,
			double adj_close, double adj_open, double adj_volume, double split_factor, String symbol, String exchange,
			String date) {
		super();
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.volume = volume;
		this.adj_high = adj_high;
		this.adj_low = adj_low;
		this.adj_close = adj_close;
		this.adj_open = adj_open;
		this.adj_volume = adj_volume;
		this.split_factor = split_factor;
		this.symbol = symbol;
		this.exchange = exchange;
		this.date = date;
	}
	public Data() {}
	public double getOpen() {
		return open;
	}
	public void setOpen(double open) {
		this.open = open;
	}
	public double getHigh() {
		return high;
	}
	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}
	public void setLow(double low) {
		this.low = low;
	}
	public double getClose() {
		return close;
	}
	public void setClose(double close) {
		this.close = close;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public double getAdj_high() {
		return adj_high;
	}
	public void setAdj_high(double adj_high) {
		this.adj_high = adj_high;
	}
	public double getAdj_low() {
		return adj_low;
	}
	public void setAdj_low(double adj_low) {
		this.adj_low = adj_low;
	}
	public double getAdj_close() {
		return adj_close;
	}
	public void setAdj_close(double adj_close) {
		this.adj_close = adj_close;
	}
	public double getAdj_open() {
		return adj_open;
	}
	public void setAdj_open(double adj_open) {
		this.adj_open = adj_open;
	}
	public double getAdj_volume() {
		return adj_volume;
	}
	public void setAdj_volume(double adj_volume) {
		this.adj_volume = adj_volume;
	}
	public double getSplit_factor() {
		return split_factor;
	}
	public void setSplit_factor(double split_factor) {
		this.split_factor = split_factor;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	private String date;
}
