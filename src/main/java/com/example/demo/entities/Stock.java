package com.example.demo.entities;

// import java.util.List;

//import java.sql.Date;

public class Stock {
	//public static List<Stock> pendingOrders;
	// defines the stock entity and gets/sets its parameters
	private int id;
	private String date;
	private String stockTicker;
	private double price;
	private long quantity;
	private String buyOrSell;
	private int statusCode;
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public Stock() {
	}

	public Stock(int id, String date, String stockTicker, double price, long quantity, String buyOrSell,int statusCode) {
		super();
		this.id = id;
		this.date = date;
		this.stockTicker = stockTicker;
		this.price = price;
		this.quantity = quantity;
		this.statusCode = statusCode;
		this.buyOrSell = buyOrSell;
	}
	
}

