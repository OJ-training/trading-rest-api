package com.example.demo.entities;
import java.util.*;

public class CompanyData {
	private Pagination pagination;
	private List<Data> data;
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
	public CompanyData(Pagination pagination, List<Data> data) {
		super();
		this.pagination = pagination;
		this.data = data;
	}
	public CompanyData() {}
	public List<Data> getData() {
		return data;
	}
	public void setData(List<Data> data) {
		this.data = data;
	}
}
