package com.example.demo.entities;

public class Statement {
	double maxProfit;
	double maxLoss;
	double totalProfit;
	double totalLoss;
	
	public Statement() {}
	public Statement(double maxProfit, double maxLoss, double totalProfit, double totalLoss) {
		super();
		this.maxProfit = maxProfit;
		this.maxLoss = maxLoss;
		this.totalProfit = totalProfit;
		this.totalLoss = totalLoss;
	}
	public double getMaxProfit() {
		return maxProfit;
	}
	public void setMaxProfit(double maxProfit) {
		this.maxProfit = maxProfit;
	}
	public double getMaxLoss() {
		return maxLoss;
	}
	public void setMaxLoss(double maxLoss) {
		this.maxLoss = maxLoss;
	}
	public double getTotalProfit() {
		return totalProfit;
	}
	public void setTotalProfit(double totalProfit) {
		this.totalProfit = totalProfit;
	}
	public double getTotalLoss() {
		return totalLoss;
	}
	public void setTotalLoss(double totalLoss) {
		this.totalLoss = totalLoss;
	}

}
